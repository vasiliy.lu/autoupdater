unit uAutoUpdater;

interface

uses
  System.SysUtils, System.Classes, System.UITypes,
  Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, Vcl.Dialogs, uWebUpdater,
  uWebUpdater.Version;

type
  TAutoUpdaterOption = (uoCheckOnCreate, uoMandatoryUpdate, uoAuto);
  // uAuto - �������������� ���������� ��� ������

  TAutoUpdaterOptions = set of TAutoUpdaterOption;

  OnAfterCheckEvent = procedure(Sender: TObject; const Update: boolean)
    of object;

  EUpdateException = class(Exception)

  end;

  TCustomAutoUpdaterWin = class(ThWebUpdaterWin)
  private
    // FUpdateFoundText: string;
    FOptions: TAutoUpdaterOptions;
    FCancelMessage: string;
    FOnAfterCheck: OnAfterCheckEvent;
    FSaveFormCreate: TNotifyEvent;
    FActive: boolean;
    FThereIsUpdate: string;
    function GetForm: TForm;
    procedure SetOptions(const Value: TAutoUpdaterOptions);
    procedure SetCancelMessage(const Value: string);
    procedure RestoreEvents; // ������������ ������� �� �����������
    procedure SetEvents; // ���������� ����������
    // procedure SetUpdateFoundText(const Value: string);
    procedure FormCreate(Sender: TObject);
    procedure DoOnAfterCheck(const AUpdating: boolean);
    procedure SetActive(const Value: boolean);
    procedure SetCurrentVersionStr(const Value: string);
    function GetCurrentVersionStr: string;
    procedure SetThereIsUpdate(const Value: string);
  protected
    procedure Loaded; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property Options: TAutoUpdaterOptions read FOptions write SetOptions
      default [uoCheckOnCreate];
    // property UpdateFoundText: string read FUpdateFoundText write SetUpdateFoundText; //
    property CancelMessage: string read FCancelMessage write SetCancelMessage;
    property ThereIsUpdateMessage: string read FThereIsUpdate
      write SetThereIsUpdate;
    property OnAfterCheck: OnAfterCheckEvent read FOnAfterCheck
      write FOnAfterCheck;
    property Active: boolean read FActive write SetActive default false;
    property CurrentVersionText: string read GetCurrentVersionStr
      write SetCurrentVersionStr;
    procedure AutoCheckUpdate;
  end;

  TAutoUpdaterWin = class(TCustomAutoUpdaterWin)
  published
    // property SetupInfoURL: string;
    property Active;
    property Options;
    property CancelMessage;
    property ThereIsUpdateMessage;
    property OnAfterCheck;
    property CurrentVersionText;
  end;

const
  CANCEL_MESSAGE_DEF
    : string = '�� ������ �������� ��������� � ����� ������� �����.';

implementation

{ TCustomAutoUpdaterWin }

procedure TCustomAutoUpdaterWin.AutoCheckUpdate;
  procedure TerminateApp;
  begin
    Application.ShowMainForm := false;
    Application.Terminate;
    raise EUpdateException.Create('Software update was aborted by user');
  end;

begin
  if not FActive then
  begin
    DoOnAfterCheck(false);
    exit;
  end;
  CheckUpdates;
  if AvailableUpdate then
  begin
    if (uoAuto in FOptions) or (MessageDlg(FThereIsUpdate, mtConfirmation,
      [mbOk, mbCancel], 0) = mrOK) then
    begin
      try
        Update;
      except
        on e: Exception do
        begin
          Showmessage(e.Message);
          if uoMandatoryUpdate in FOptions then
          begin
            TerminateApp;
          end
          else
            DoOnAfterCheck(false);
        end;
      end;
    end
    else
    begin
      if uoMandatoryUpdate in FOptions then
      begin
        TerminateApp;
      end
      else
        Showmessage(FCancelMessage);
      DoOnAfterCheck(false);
    end;
  end
  else
    DoOnAfterCheck(false);
end;

constructor TCustomAutoUpdaterWin.Create(AOwner: TComponent);
begin
  inherited;
  FOptions := [uoCheckOnCreate];
  FActive := false;
  // if not csDesigning in ComponentState then
  SetCancelMessage(CANCEL_MESSAGE_DEF);
  AppHandle := Application;
  //
  FThereIsUpdate := 'The software update is available. Update now?';
end;

destructor TCustomAutoUpdaterWin.Destroy;
begin
  if not(csDesigning in ComponentState) then
    RestoreEvents;
  inherited;
end;

procedure TCustomAutoUpdaterWin.DoOnAfterCheck(const AUpdating: boolean);
begin
  if Assigned(FOnAfterCheck) then
    FOnAfterCheck(self, AUpdating);
end;

procedure TCustomAutoUpdaterWin.FormCreate(Sender: TObject);
begin
  // ��������� �������� � ownerform.oncreate
  if Assigned(FSaveFormCreate) then
    try
      FSaveFormCreate(Sender);
    except
      on e: Exception do
        Application.HandleException(e);
    end;
  // ��������� ����������
  AutoCheckUpdate;

end;

function TCustomAutoUpdaterWin.GetCurrentVersionStr: string;
begin
  Result := CurrentVersion.Text
end;

function TCustomAutoUpdaterWin.GetForm: TForm;
begin
  if Owner is TCustomForm then
    Result := TForm(Owner as TCustomForm)
  else
    Result := nil;
end;

procedure TCustomAutoUpdaterWin.Loaded;
var
  Loading: boolean;
begin
  Loading := csLoading in ComponentState;
  inherited Loaded;
  if not(csDesigning in ComponentState) then
  begin
    if Loading then
      SetEvents;
  end;
end;

procedure TCustomAutoUpdaterWin.RestoreEvents;
begin
  // if (Owner <> nil) and (Owner is TCustomForm) then
  // with TForm(Form) do
  // begin
  // OnShow := FSaveFormShow;
  // OnCloseQuery := FSaveFormCloseQuery;
  // OnDestroy := FSaveFormDestroy;
  // end;
end;

procedure TCustomAutoUpdaterWin.SetActive(const Value: boolean);
begin
  FActive := Value;
end;

procedure TCustomAutoUpdaterWin.SetCancelMessage(const Value: string);
begin
  FCancelMessage := Value;
end;

procedure TCustomAutoUpdaterWin.SetCurrentVersionStr(const Value: string);
begin
  CurrentVersion.Text := Value;
end;

procedure TCustomAutoUpdaterWin.SetEvents;
begin
  // inherited;
  if Owner is TCustomForm then
  begin
    with TForm(GetForm) do
    begin
      FSaveFormCreate := OnCreate;
      OnCreate := FormCreate;
    end;
  end;
end;

procedure TCustomAutoUpdaterWin.SetOptions(const Value: TAutoUpdaterOptions);
begin
  FOptions := Value;
end;

procedure TCustomAutoUpdaterWin.SetThereIsUpdate(const Value: string);
begin
  FThereIsUpdate := Value;
end;

end.
