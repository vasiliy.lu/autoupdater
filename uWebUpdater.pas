unit uWebUpdater;

interface

uses
  System.SysUtils, System.Classes, System.Inifiles, System.JSON,
  uWebUpdater.Version,
  Vcl.Forms, Winapi.Windows, Winapi.Shellapi, IdHTTP, IdSSLOpenSSL;

// [SetupName].json format
// {
// "SETUP_URL":"http://example.com/files/ExampleSetup.exe",
// "VER_MAJOR":1,
// "VER_MINOR":0,
// "VER_BUILD":0
// }

type
  TUpdateStatus = (usLoad, usInstall, usDone);

  ThUpdateInfo = class
  private
    FVersion: ThVer;
    FSetupURL: string;
    FWhatIsNew: TStringList;
    procedure SetSetupURL(const Value: string);
    procedure SetVersion(const Value: ThVer);
    function GetWhatIsNew: TStrings;
    procedure SetWhatIsNew(const Value: TStrings);
  public
    constructor Create; overload;
    constructor Create(const AJSONText: string); overload;
    destructor Destroy; override;

    property Version: ThVer read FVersion write SetVersion;
    property SetupURL: string read FSetupURL write SetSetupURL;
    property WhatIsNew: TStrings read GetWhatIsNew write SetWhatIsNew;
    procedure LoadFromStream(AStream: TStream);
    procedure LoadFromJSON(const AJSONText: string);
    function SaveToJSON: string;
    procedure SaveToFile(const AFileName: string);
  end;

  TBeforeUpdateEvent = procedure(Sender: TObject; var CanUpdate: boolean)
    of object;
  TOnUpdateEvent = procedure(Sender: TObject; const Status: TUpdateStatus;
    const Done: real) of object;

  ThCustomWebUpdater = class(TComponent)
  protected
    procedure DoBeforeUpdate(var CanUpdate: boolean); virtual;
    procedure DoOnUpdateProcess(const Status: TUpdateStatus; const Done: real);
    procedure RunInstall(const AFileName: string); virtual;
    procedure TerminateApplication; virtual;

  private
    //
    FWebUpdateInfo: ThUpdateInfo;
    FCurrentVersion: ThVer;
    FSetupInfoURL: string;
    FBeforeUpdate: TBeforeUpdateEvent;
    FOnUpdateProcess: TOnUpdateEvent;
    FAppName: string;
    procedure wdOnError(Sender: TObject; AEx: Exception);
    procedure SetCurrentVersion(const Value: ThVer);
    procedure SetSetupInfoURL(const Value: string);
    function GetAvailableUpdate: boolean; // ���������� �� ���������� � �������
    function DownloadSetupFile: string;
    //
    procedure SetAppName(const Value: string);
    //
    procedure DownloadOnProgress(Sender: TObject; Done: real);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property AppName: string read FAppName write SetAppName;
    property CurrentVersion: ThVer read FCurrentVersion write SetCurrentVersion;
    //
    property WebUpdateInfo: ThUpdateInfo read FWebUpdateInfo;
    //
    property SetupInfoURL: string read FSetupInfoURL write SetSetupInfoURL;
    // url ��� ��������� json �����
    property AvailableUpdate: boolean read GetAvailableUpdate;
    function CheckUpdates: boolean;
    // ��������� ����������, true - ���� ����� ������
    procedure Update;
    // ��������� ����������
    property BeforeUpdate: TBeforeUpdateEvent read FBeforeUpdate
      write FBeforeUpdate;
    // ������� ����� �����������
    property OnUpdateProcess: TOnUpdateEvent read FOnUpdateProcess
      write FOnUpdateProcess;
  end;

  ThWebUpdater = class(ThCustomWebUpdater)
  published
    property AppName;
    property SetupInfoURL;
    property BeforeUpdate;
    property OnUpdateProcess;
  end;

  ThWebUpdaterWin = class(ThWebUpdater)
  private
    FApp: TApplication;
    FSUPPRESSMSGBOXES: boolean;
    procedure SetAppHandle(const Value: TApplication);
    procedure SetSUPPRESSMSGBOXES(const Value: boolean);
  protected
    procedure RunInstall(const AFileName: string); override;
    procedure TerminateApplication; override;
  public
    constructor Create(AOwner: TComponent); override;

    property AppHandle: TApplication read FApp write SetAppHandle;
    property SUPPRESSMSGBOXES: boolean read FSUPPRESSMSGBOXES
      write SetSUPPRESSMSGBOXES;
  end;

function GetAppDataPath: string;

const
  JSONFILENAME = 'UpdateInfo.json';
  SETUPFILENAME = 'Setup.exe';
  V_PROGRAMDATA = 'appdata';

implementation

uses
  Winapi.Shlobj, Winapi.ActiveX;

procedure ShellExecuteGS(const AWnd: HWND; const AOperation, AFileName: String;
  const AParameters: String = ''; const ADirectory: String = '';
  const AShowCmd: Integer = SW_SHOWNORMAL; AWait: boolean = false);
var
  ExecInfo: TShellExecuteInfo;
  NeedUninitialize: boolean;
begin
  Assert(AFileName <> '');

  NeedUninitialize := SUCCEEDED(CoInitializeEx(nil, COINIT_APARTMENTTHREADED or
    COINIT_DISABLE_OLE1DDE));
  try
    FillChar(ExecInfo, SizeOf(ExecInfo), 0);
    ExecInfo.cbSize := SizeOf(ExecInfo);

    ExecInfo.Wnd := AWnd;
    ExecInfo.lpVerb := Pointer(AOperation);
    ExecInfo.lpFile := PChar(AFileName);
    ExecInfo.lpParameters := Pointer(AParameters);
    ExecInfo.lpDirectory := Pointer(ADirectory);
    ExecInfo.nShow := AShowCmd;
    if AWait then
      ExecInfo.fMask := SEE_MASK_NOCLOSEPROCESS
    else
      ExecInfo.fMask := SEE_MASK_NOASYNC or SEE_MASK_FLAG_NO_UI;
{$IFDEF UNICODE}
    // �������������, ��. http://www.transl-gunsmoker.ru/2015/01/what-does-SEEMASKUNICODE-flag-in-ShellExecuteEx-actually-do.html
    ExecInfo.fMask := ExecInfo.fMask or SEE_MASK_UNICODE;
{$ENDIF}
{$WARN SYMBOL_PLATFORM OFF}
    Win32Check(ShellExecuteEx(@ExecInfo));
{$WARN SYMBOL_PLATFORM ON}
    if AWait then
      WaitForSingleObject(ExecInfo.hProcess, INFINITE);
  finally
    if NeedUninitialize then
      CoUninitialize;
  end;
end;

function GetAppDataPath: string;
var
  s: PChar;
begin
  Result := GetEnvironmentVariable(V_PROGRAMDATA);
  if Result = '' then
  begin
    GetMem(s, 255 * sizeof(char));
    try
      SHGetFolderPath(0, CSIDL_APPDATA, 0, 0, s);
      Result := s;
    finally
      FreeMem(s);
    end;
  end;
  if (Result.Length > 0) and (Result[Result.Length] <> '\') then
    Result := Result + '\';
end;

{ ThUpdateInfo }

constructor ThUpdateInfo.Create(const AJSONText: string);
begin
  Create;
  LoadFromJSON(AJSONText);
end;

constructor ThUpdateInfo.Create;
begin
  FWhatIsNew := TStringList.Create;
  FVersion.Raw := 0;
end;

destructor ThUpdateInfo.Destroy;
begin
  FreeAndNil(FWhatIsNew);
  inherited;
end;

function ThUpdateInfo.GetWhatIsNew: TStrings;
begin
  Result := FWhatIsNew;
end;

procedure ThUpdateInfo.LoadFromJSON(const AJSONText: string);
var
  o: TJSONObject;
  intval: TJSONNumber;
  strval: TJSONString;
  v: TJSONValue;
begin
  v := TJSONObject.ParseJSONValue(AJSONText);
  if v = nil then
    exit;

  o := v as TJSONObject;
  try
    if o.TryGetValue('VER_MAJOR', intval) then
      FVersion.Major := intval.AsInt;
    if o.TryGetValue('VER_MINOR', intval) then
      FVersion.Minor := intval.AsInt;
    if o.TryGetValue('VER_BUILD', intval) then
      FVersion.Build := intval.AsInt;
    if o.TryGetValue('SETUP_URL', strval) then
      FSetupURL := strval.Value;
    // FVersion.Major := (o.Values['VER_MAJOR'] as TJSONNumber).AsInt;
    // FVersion.Minor := (o.Values['VER_MINOR'] as TJSONNumber).AsInt;
    // FVersion.Build := (o.Values['VER_BUILD'] as TJSONNumber).AsInt;
    // if Assigned(intval) then
    // intval.Free;
    // if Assigned(strval) then
    // strval.Free;
  finally
    o.Free;
  end;
end;

procedure ThUpdateInfo.LoadFromStream(AStream: TStream);
var
  sl: TStringStream;
begin
  sl := TStringStream.Create;
  try
    sl.CopyFrom(AStream, AStream.Size);
    LoadFromJSON(sl.DataString);
  finally
    sl.Free;
  end;
end;

procedure ThUpdateInfo.SaveToFile(const AFileName: string);
begin

end;

function ThUpdateInfo.SaveToJSON: string;
begin

end;

procedure ThUpdateInfo.SetSetupURL(const Value: string);
begin
  FSetupURL := Value;
end;

procedure ThUpdateInfo.SetVersion(const Value: ThVer);
begin
  FVersion := Value;
end;

procedure ThUpdateInfo.SetWhatIsNew(const Value: TStrings);
begin

end;

{ ThWebUpdater }

function GetFile(const AURL: string; OutStream: TStream): boolean;
var
  http: TIdHTTP;
  ssl: TIdSSLIOHandlerSocketOpenSSL;
begin
  { Fd := ThWebDownloader.Create;
    Fd.OnProgress := DownloadOnProgress;
    Fd.OnError  := wdOnError; }
  http := TIdHTTP.Create;
  try
    ssl:= TIdSSLIOHandlerSocketOpenSSL.Create(http);
    ssl.SSLOptions.SSLVersions := [sslvSSLv2, sslvSSLv23, sslvSSLv3, sslvTLSv1,sslvTLSv1_1,sslvTLSv1_2];
    http.IOHandler := ssl;
    http.ConnectTimeout := 3000;
    try
      http.Get(AURL, OutStream);
    except
      on e: Exception do
      begin
        e.Message := 'Error on check updates: '+e.Message;
        Application.ShowException(e);
      end;
    end;
  finally
    http.Free;
  end;
end;

function ThCustomWebUpdater.CheckUpdates: boolean;
var
  f: TFileStream;
  fn: string;

begin
  // get appdata path
  fn := GetAppDataPath + FAppName + '\' + JSONFILENAME;
  // get json file
  if FileExists(fn) then
    f := TFileStream.Create(fn, fmOpenReadWrite)
  else
  begin
    ForceDirectories(ExtractFilePath(fn));
    f := TFileStream.Create(fn, fmCreate);
  end;
  try
    f.Size := 0;

    GetFile(FSetupInfoURL, f);
    //
    f.Position := 0;
    FWebUpdateInfo.LoadFromStream(f);
    Result := GetAvailableUpdate;
    //
  finally
    f.Free;
  end;
end;

constructor ThCustomWebUpdater.Create(AOwner: TComponent);
begin
  inherited;

  //
  FWebUpdateInfo := ThUpdateInfo.Create;
  FCurrentVersion.Raw := 0;
  FSetupInfoURL := '';
  FAppName := '';
end;

destructor ThCustomWebUpdater.Destroy;
begin
  FreeAndNil(FWebUpdateInfo);
  inherited;
end;

procedure ThCustomWebUpdater.DoBeforeUpdate(var CanUpdate: boolean);
begin
  if Assigned(FBeforeUpdate) then
    FBeforeUpdate(self, CanUpdate);
end;

procedure ThCustomWebUpdater.DoOnUpdateProcess(const Status: TUpdateStatus;
  const Done: real);
begin
  if Assigned(FOnUpdateProcess) then
    FOnUpdateProcess(self, Status, Done);
  Application.ProcessMessages;
end;

procedure ThCustomWebUpdater.DownloadOnProgress(Sender: TObject; Done: real);
begin
  DoOnUpdateProcess(usLoad, Done);
end;

function ThCustomWebUpdater.DownloadSetupFile: string;
var
  f: TFileStream;
begin
  // get appdata path
  Result := GetAppDataPath + FAppName + '\' + SETUPFILENAME;
  // get setup file
  if FileExists(Result) then
    f := TFileStream.Create(Result, fmOpenReadWrite)
  else
    f := TFileStream.Create(Result, fmCreate);
  try
    f.Size := 0;
    GetFile(FWebUpdateInfo.FSetupURL, f)
    //
  finally
    f.Free;
  end;
end;

function ThCustomWebUpdater.GetAvailableUpdate: boolean;
begin
  Result := FWebUpdateInfo.FVersion.Raw > FCurrentVersion.Raw;
end;

procedure ThCustomWebUpdater.RunInstall(const AFileName: string);
begin
end;

procedure ThCustomWebUpdater.SetAppName(const Value: string);
begin
  FAppName := Value;
end;

procedure ThCustomWebUpdater.SetCurrentVersion(const Value: ThVer);
begin
  FCurrentVersion := Value;
end;

procedure ThCustomWebUpdater.SetSetupInfoURL(const Value: string);
begin
  FSetupInfoURL := Value;
end;

procedure ThCustomWebUpdater.TerminateApplication;
begin
end;

procedure ThCustomWebUpdater.Update;
var
  FCanUpdate: boolean;
  FSetupFileName: string;
begin
  if CheckUpdates then
  begin
    FCanUpdate := true;
    DoBeforeUpdate(FCanUpdate);
    if FCanUpdate then
    begin
      // 1. download setup file
      DoOnUpdateProcess(usLoad, 0);
      FSetupFileName := DownloadSetupFile;
      DoOnUpdateProcess(usLoad, 1);
      DoOnUpdateProcess(usInstall, 0);
      // 2. shellexec
      RunInstall(FSetupFileName);
      // 3. terminate
      TerminateApplication;
    end;

  end;
end;

procedure ThCustomWebUpdater.wdOnError(Sender: TObject; AEx: Exception);
begin
  Application.ShowException(AEx);
end;

{ ThWebUpdaterWin }

constructor ThWebUpdaterWin.Create(AOwner: TComponent);
begin
  inherited;
  FSUPPRESSMSGBOXES := true;
end;

procedure ThWebUpdaterWin.RunInstall(const AFileName: string);
var
  p: string;
begin
  p := '/SP- /SILENT  /NOCANCEL /NORESTART /CLOSEAPPLICATIONS';
  if FSUPPRESSMSGBOXES then
    p := p + ' /SUPPRESSMSGBOXES';

  ShellExecuteGS(FApp.Handle, 'open', PChar(AFileName), PChar(p), '',
    SW_SHOWNORMAL);
end;

procedure ThWebUpdaterWin.SetAppHandle(const Value: TApplication);
begin
  FApp := Value;
end;

procedure ThWebUpdaterWin.SetSUPPRESSMSGBOXES(const Value: boolean);
begin
  FSUPPRESSMSGBOXES := Value;
end;

procedure ThWebUpdaterWin.TerminateApplication;
begin
  FApp.Terminate;
end;

end.
