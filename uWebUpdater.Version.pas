unit uWebUpdater.Version;

interface

uses
  System.SysUtils;

type
  ThVer = packed record
    Raw: cardinal;
    function GetText: string;
  end;

  ThVerHelper = record helper for ThVer
  private
    // function GetMajor: byte;
    // function GetMinor: byte;
    // procedure SetMajor(const AValue: byte);
    // procedure SetMinor(const AValue: byte);
    function _GetText: string;
    procedure SetText(const AValue: string);
    // function GetBuild: byte;
    // procedure SetBuild(const Value: byte);
    procedure SetValue(const Index: integer; const Value: word);
    function GetValue(const Index: integer): word;
    // function GetValue(const OffsetBit,AndMask: word): word;
  public
    // low word - mask, high word - offset
    property Major: word index $1800FF read GetValue write SetValue;
    property Minor: word index $1000FF read GetValue write SetValue;
    property Build: word index $FFFF read GetValue write SetValue;
    property Text: string read _GetText write SetText;
    class function Create(const AMajor, AMinor, ABuild: byte)
      : ThVer;overload; static;
    class function Create(const ARaw: cardinal)
      : ThVer;overload; static;
    procedure GetCurrent;
  end;

implementation


{ TModuleVerHelper }

class function ThVerHelper.Create(const AMajor, AMinor, ABuild: byte)
  : ThVer;
begin
  Result.Raw := 0;

  Result.Major := AMajor;
  Result.Minor := AMinor;
  Result.Build := ABuild;
end;


class function ThVerHelper.Create(const ARaw: cardinal): ThVer;
begin
  Result.Raw := ARaw;
end;

procedure ThVerHelper.GetCurrent;
begin
  self.Raw := GetFileVersion(ParamStr(0));
end;

function ThVerHelper._GetText: string;
begin
  Result := format('%d.%d.%d', [Major, Minor, Build]);
end;

function ThVerHelper.GetValue(const Index: integer): word;
begin
  Result := (Raw shr (Index shr 16)) and (Index and $FFFF);
end;

procedure ThVerHelper.SetText(const AValue: string);
var
  a: TArray<string>;
begin
  a := AValue.Split(['.']);
  if length(a) = 1 then
    self.Minor := a[0].ToInteger()
  else if length(a) = 2 then
  begin
    self.Major := a[0].ToInteger();
    self.Minor := a[1].ToInteger()
  end
  else if length(a) = 3 then
  begin
    self.Major := a[0].ToInteger();
    self.Minor := a[1].ToInteger();
    self.Build := a[2].ToInteger();
  end
  else
    Raw := 0;
  Finalize(a);
end;

procedure ThVerHelper.SetValue(const Index: integer; const Value: word);
var
  offs: word;
begin
  offs := (Index shr 16);
  Raw := (Raw and ($FFFFFFFF xor ((Index and $FFFF) shl offs))) or
    (Value shl offs);
end;

{ ThVer }

function ThVer.GetText: string;
begin
  Result := self._GetText;
end;

end.
