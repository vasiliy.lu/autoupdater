unit fAutoUpdaterServer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IdContext, IdCustomHTTPServer,
  Vcl.StdCtrls, IdBaseComponent, IdComponent, IdCustomTCPServer, IdHTTPServer;

type
  TForm1 = class(TForm)
    IdHTTPServer1: TIdHTTPServer;
    Button1: TButton;
    edDir: TEdit;
    Label1: TLabel;
    Button2: TButton;
    Memo1: TMemo;
    procedure IdHTTPServer1CommandGet(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure UpdateButtons;

  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  IdHTTPServer1.Active := true;

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  IdHTTPServer1.Active := false;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  root_dir: string;
  h_port: string;
begin
  if FindCmdLineSwitch('d',root_dir) then
    edDir.Text := root_dir;
  if FindCmdLineSwitch('p',h_port) then
    IdHTTPServer1.DefaultPort:= h_port.ToInteger;
  if FindCmdLineSwitch('s') then
    IdHTTPServer1.Active := true;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  UpdateButtons;
end;

procedure TForm1.IdHTTPServer1CommandGet(AContext: TIdContext;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);

  function ReplaceSlash(const AFN: string): string;
  var
    i: integer;
  begin
    Result := '';
    for i := 2 to AFN.Length do
      if AFN[i] = '/' then
        Result := Result + '\'
      else
        Result := Result + AFN[i];
  end;

begin
  //
  if ExtractFileExt(ARequestInfo.Document) <> '' then
    AResponseInfo.ServeFile(AContext, IncludeTrailingPathDelimiter(edDir.Text)+ ReplaceSlash(ARequestInfo.Document));
end;

procedure TForm1.UpdateButtons;
begin
  Button1.Enabled := not IdHTTPServer1.Active;
  Button2.Enabled := IdHTTPServer1.Active;
end;

end.
